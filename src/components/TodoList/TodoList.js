import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import React from 'react';
import ElemList from './ElementList';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        display: 'flex', 
        justifyContent: 'center',
        // mx: 'auto',
    },
    paper: {
        padding:0,
        margin: "20px auto 20px",
        display:'flex',
        justifyContent:'space-between',
        color: theme.palette.text.secondary,
        listStyle: 'none',
        flexDirection: 'column',
        width:350,
        maxWidth: '50%',
    },
  }));

const TodoList = (props) => {

    const classes = useStyles();
    const taskslist = props.tasks.map((task, index) => (
            <ElemList id={task.id} text={task.text} isDone={task.isDone} invertDone={props.invertDone} />
        )
    );
    return (
    <Paper
    component={List}
    className={classes.paper}
    >
        {taskslist}
    </Paper>
    );
}



export default TodoList;