import React, { useState, useEffect } from 'react';
import { ListItem, ListItemText, makeStyles } from '@material-ui/core';

const styles = makeStyles((theme) => ({
    checkbox: {
        visibility: 'hidden',
    },
    listItem:{
        textAlign: 'center',
        margin:0,
        backgroundColor: theme.palette.success.light,

    },
    listItemActive:{
        backgroundColor:'green',
    },
}));

const ElemList = ({id, text, isDone, invertDone}) => 
{
    const [color, setcolor] = useState(() => (isDone ? '#81c784' : '#e57373'));
    const classes = styles();

    const invertCheckBox = (e) => {
        invertDone(id);  
    };

    useEffect(() => {
        setcolor(() => (isDone ? '#81c784' : '#e57373'));
    }, [isDone]);
    
    return (
        <ListItem 
        button 
        key={id} 
        onClick={(e) => {
                invertCheckBox(e);
            }} 
        className={classes.listItem}
        style={{
            backgroundColor: color,
        }}
        >
            <ListItemText primary={text} />
        </ListItem>
    )
};

export default ElemList;