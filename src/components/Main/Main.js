import { Box, Button, Container } from "@material-ui/core";

import React from "react";

const Main = () => 
{
    return (
    <Container maxWidth="md">
        <Box
        sx={{
            display: 'flex',
        }}>
            <Button
            variant="contained"
            >
                Add
            </Button>
        </Box>
    </Container>
    );
}

export default Main;