import React, { useState } from 'react';
import { Paper, IconButton, TextField } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
      flexGrow: 1,
      display: 'flex', 
      justifyContent: 'center',
      marginTop:20,
  },
  paper: {
      padding: theme.spacing(2),
      margin: "20px auto 20px",
      textAlign: "center",
      display:'flex',
      justifyContent:'space-between',
      color: theme.palette.text.secondary,
      listStyle: 'none',
      width:350,
      maxWidth: '50%',
  },
}));

const AddTask = (props) => {
  const [value, setValue] = useState("");

  const handleChange = e => {
    setValue(e.target.value);
  }


  const handleAddtask = e => {
    if (value !== "")
      props.onAddTask({ text: value, id: value, isDone: false });
    setValue("");
  }
  const classes = useStyles();

  return (
      <Paper
        className={classes.paper}
        >
        <TextField
          placeholder="Введите название задачи"
          variant="filled"
          label="Введите название задачи"
          id="addTask"
          value={value}
          multiline
          size="small"
          onChange={handleChange}
        />
        <IconButton
          color="primary"
          onClick={handleAddtask}
          size="medium"
          aria-label="add"
        >
          <Add />
        </IconButton>
      </Paper>
  );
};

export default AddTask;