import React, { useState } from 'react';
import { Drawer, Toolbar, Divider, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import Mail from '@material-ui/icons/Mail';
import Inbox from '@material-ui/icons/Mail';
import { makeStyles } from "@material-ui/core/styles";
import { Link } from 'react-router-dom';
const useStyles = makeStyles((theme)=>({
    root: {
        width: "20%",
        flexShrink: 0,
        '& .MuiDrawer-paper': {
            width: "15%",
            boxSizing: 'border-box',
        },
    }
}));

const Menu = (props) => {
    const clasess = useStyles();
    const [selectedId, setselectedId] = useState(0);
    const handleListItemClick = (event, index) => {
      setselectedId(index);
    };
    return (
    <Drawer
        variant="permanent"
        anchor="left"
        className={clasess.root}
    >
        <Toolbar />
        <Divider />
        <List>
            {props.menus.map(({text, href}, index) => (
              <Link to={href}>
                  <ListItem 
                  button 
                  key={text} 
                  selected={selectedId === index}
                  onClick={(event) => handleListItemClick(event, index)}
                  >
                    <ListItemText primary={text} />
                  </ListItem>
                  {/* <span>{text}</span> */}
              </Link>
            ))}
        </List>
      </Drawer>
    );
};

export default Menu;