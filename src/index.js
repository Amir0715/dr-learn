import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import reportWebVitals from './reportWebVitals';
import App from './App';
import { CssBaseline } from '@material-ui/core';
import { BrowserRouter, Redirect } from 'react-router-dom';
// const theme = {
//   colors: {
//     primary: 'tomato',
//     accent: 'yellow',
//   },
// };
ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter >
    <CssBaseline />
    <App />
    </BrowserRouter >
  </React.StrictMode>,
  document.getElementById('root')
);
reportWebVitals();
