import TodoList from '../components/TodoList/TodoList';
import AddTask from '../components/AddTask/AddTask';
import { Box } from '@material-ui/core';
import React, {useEffect, useState} from 'react';
const Todo = () => {
    const [todos, setTodos] = useState([]);
    useEffect(() => {
        const data = window.localStorage.getItem('todos');
        console.log(data);
        if (data) {
          const ser = JSON.parse(data);
          console.log(ser);
          setTodos(ser);
        }
    }, [])
    const addTask = (todo) => {
      setTodos([...todos, todo]);
    };
  
    useEffect(() => {
      window.localStorage.setItem('todos', JSON.stringify(todos));
    }, [todos])
    const invertDoneTask = (currentId) => {
        setTodos((prevTodo) => {
          return prevTodo.map((todo) => {
            
            if(todo.id === currentId) {
              todo.isDone = !todo.isDone;
            }
            return todo;
          });
        });
      };
    return (
        <Box>
            <TodoList tasks={todos} invertDone={invertDoneTask}/>
            <AddTask onAddTask={addTask} />
        </Box>
    );
};

export default Todo;