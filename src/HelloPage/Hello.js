import { Box } from '@material-ui/core';
import React from 'react';

const Hello = () => {
    return (
        <Box component="span" style={{margin:"auto", color:"red", backgroundColor:"aqua", width:'100px', height:"50px", display:"block"}}>
            Hello world
        </Box>
    );
};

export default Hello;