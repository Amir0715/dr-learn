import React, { useState } from 'react';
import Todo from './TodoPage/Todo';
import Menu from './components/Menu/Menu';
import Hello from './HelloPage/Hello';
import {
  Route,
  Switch,
  Redirect,
  withRouter
} from "react-router-dom";

const App = () => {



  const [menus, setMenu] = useState([
    {text: 'Todo', href:'/home'}, 
    {text: 'Hello', href:'/hello'}, 
    {text: 'Cliker', href:'/hello'}, 
    {text: 'Inbox', href:'/hello'}
  ]);

  return (
    <div className="App">
      <Menu menus={menus} />
      <Switch>
        <Route path='/home' component={Todo} />
        <Route path='/hello' component={Hello} />

      </Switch>
    </div>
  );
}

export default withRouter(App);
